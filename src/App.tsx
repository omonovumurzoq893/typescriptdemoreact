import { ChangeEvent, useEffect, useState } from "react";
import styles from "./styles/App.module.css";
import { IData } from "./interfaces";
import { data } from "./constants";

function App(): JSX.Element {
  useEffect(() => {
    setArr(data);
  }, []);
  const [title, setTitle] = useState<string>("");
  const [arr, setArr] = useState<IData[]>(data);
  const changeHandler = (evt: ChangeEvent<HTMLInputElement>) => {
    setTitle(evt.target.value);
  };
  const handleSubmit = (): void => {
    if (!title?.length) return;
    const newData = {
      title: title,
      id: new Date().getTime(),
      description: "description",
    };
    setArr([...arr, newData]);
    setTitle("");
  };

  const deleteFunc = (id: number): void => {
    const newArr: IData[] = arr.filter((c) => c.id !== id);
    setArr(newArr);
  };

  return (
    <div className={styles.todo}>
      <h1 className={styles.title}>APP todo</h1>
      <input
        placeholder="Enter todo"
        value={title}
        onChange={changeHandler}
        className={styles.input}
      />
      <button onClick={handleSubmit} className={styles.button}>
        Add todo
      </button>
      <div className={styles.card}>
        {arr?.map((c) => (
          <div key={c.id} className={styles.cardItem}>
            <p>{c.title}</p>
            <div className={styles.delBtn}>
              <button
                onClick={() => {
                  deleteFunc(c.id);
                }}>
                Del
              </button>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;
