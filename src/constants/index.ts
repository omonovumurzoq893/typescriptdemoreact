import { IData } from "../interfaces";

export const data: IData[] = [
  { title: "Tohir", id: 1, description: "Description" },
  { title: "Abdusalom", id: 2, description: "Description" },
  { title: "Asliddin", id: 3, description: "Description" },
];
